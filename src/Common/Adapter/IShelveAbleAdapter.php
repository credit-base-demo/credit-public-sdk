<?php
namespace Sdk\Common\Adapter;

use Sdk\Common\Model\IShelveAble;

interface IShelveAbleAdapter
{
    public function shelve(IShelveAble $onShelfAbleObject) : bool;

    public function offShelve(IShelveAble $onShelfAbleObject) : bool;
}
