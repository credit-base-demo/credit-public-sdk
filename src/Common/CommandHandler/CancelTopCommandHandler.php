<?php
namespace Sdk\Common\CommandHandler;

use Sdk\Common\Command\CancelTopCommand;
use Sdk\Common\Model\ITopAble;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

abstract class CancelTopCommandHandler implements ICommandHandler
{
    abstract protected function fetchITopObject($id) : ITopAble;

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction(CancelTopCommand $command)
    {
        $this->topAble = $this->fetchITopObject($command->id);

        if ($this->topAble->cancelTop()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }
}
