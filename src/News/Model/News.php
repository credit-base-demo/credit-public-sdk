<?php
namespace Sdk\News\Model;

use Sdk\Common\Model\IOperatAble;
use Sdk\Common\Model\OperatAbleTrait;

use Sdk\Common\Model\IEnableAble;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;
use Marmot\Core;
use Sdk\Common\Adapter\IOperatAbleAdapter;

use Sdk\News\Repository\NewsRepository;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class News implements IOperatAble, IObject
{
    use OperatAbleTrait, Object;

    const STATUS_NORMAL = 0;
    const STATUS_DELETE = -2;

    const CATEGORY = array(
        'POLICY_NEWS' => 1,
        'CREDIT_INFO' => 2
    );

    private $id;

    private $title;

    private $source;

    private $category;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->title = '';
        $this->source = '';
        $this->category = self::CATEGORY['POLICY_NEWS'];
        $this->statusTime = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->status = self::STATUS_NORMAL;
        $this->repository = new NewsRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->title);
        unset($this->category);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->status);
        unset($this->source);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setTitle(string $title) : void
    {
        $this->title = $title;
    }

    public function getTitle() : string
    {
        return $this->title;
    }

    public function setSource(string $source) : void
    {
        $this->source = $source;
    }

    public function getSource() : string
    {
        return $this->source;
    }

    public function setCategory(int $category) : void
    {
        $this->category = in_array(
            $category,
            self::CATEGORY
        ) ? $category : self::CATEGORY['POLICY_NEWS'];
    }

    public function getCategory() : int
    {
        return $this->category;
    }

    public function setStatus(int $status) : void
    {
        $this->status= in_array(
            $status,
            array(
                self::STATUS_NORMAL,
                self::STATUS_DELETE
            )
        ) ? $status : self::STATUS_NORMAL;
    }

    protected function getRepository() : NewsRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleRepository() : NewsRepository
    {
        return $this->getRepository();
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }
}
