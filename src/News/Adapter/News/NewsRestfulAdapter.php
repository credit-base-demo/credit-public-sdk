<?php
namespace Sdk\News\Adapter\News;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\CommonMapErrorsTrait;

use Sdk\News\Translator\NewsRestfulTranslator;
use Sdk\News\Model\NullNews;
use Sdk\News\Model\News;

use Marmot\Core;

class NewsRestfulAdapter extends GuzzleAdapter implements INewsAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;
    private $resource;

    const SCENARIOS = [
        'NEWS_LIST'=>[
            'fields'=>[],
        ],
        'NEWS_FETCH_ONE'=>[
            'fields'=>[]
        ]
    ];
    
    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new NewsRestfulTranslator(); 
        $this->resource = 'news';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getMapErrors() : array
    {
        $mapError = [];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullNews());
    }

    public function addAction(News $news) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $news,
            array('title', 'content', 'image', 'attachments', 'category', 'crew',)
        );

        $this->post(
            $this->getResource(),
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($news);
            return true;
        }

        return false;
    }

    public function editAction(News $news) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $news,
            array('title', 'content', 'image', 'attachments')
        );

        $this->patch(
            $this->getResource().'/'.$news->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($news);
            return true;
        }

        return false;
    }
}
