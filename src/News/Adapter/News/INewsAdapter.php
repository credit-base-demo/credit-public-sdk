<?php
namespace Sdk\News\Adapter\News;

use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;
use Marmot\Interfaces\IAsyncAdapter;

interface INewsAdapter extends  IFetchAbleAdapter, IAsyncAdapter, IOperatAbleAdapter
{
}
