<?php
namespace Sdk\Voucher\Translator;

use Sdk\Voucher\Model\NullVoucher;
use Sdk\Voucher\Model\Voucher;
use Sdk\Voucher\Model\IdentityInfo;
use Sdk\Common\Model\IdentifyCard;
use Marmot\Core;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Sdk\Crew\Translator\CrewRestfulTranslator;
use Sdk\Policy\Translator\PolicyRestfulTranslator;
use Sdk\Crew\Model\Crew;
use Sdk\Policy\Model\Policy;

class VoucherRestfulTranslatorTest extends TestCase
{

    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(VoucherRestfulTranslator::class)
                ->setMethods([
                    'getCrewRestfulTranslator'
                ])
                ->getMock();

        $this->childStub = new class extends VoucherRestfulTranslator {
            public function getCrewRestfulTranslator() : CrewRestfulTranslator
            {
                return parent::getCrewRestfulTranslator();
            }
        };
    }

    public function testGetCrewRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Crew\Translator\CrewRestfulTranslator',
            $this->childStub->getCrewRestfulTranslator()
        );
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new Voucher());
        $this->assertInstanceOf('Sdk\Voucher\Model\NullVoucher', $result);
    }

    private function setMethods(Voucher $expectObject, array $attributes)
    {
        if (isset($attributes['category'])) {
            $expectObject->setCategory($attributes['category']);
        }
        if (isset($attributes['template'])) {
            $expectObject->setVoucher($attributes['template']);
        }
        if (isset($attributes['createTime'])) {
            $expectObject->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $expectObject->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $expectObject->setStatus($attributes['status']);
        }

        return $expectObject;
    }

    public function testArrayToObjectCorrectObject()
    {
        $voucher = \Sdk\Voucher\Utils\MockFactory::generateVoucherArray();

        $data =  $voucher['data'];

        $actual = $this->stub->arrayToObject($voucher);

        $expectObject = new Voucher();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes);
        $this->assertEquals($expectObject, $actual);
    }

    public function testArrayToObjects()
    {
        $result = $this->stub->arrayToObjects(array());
        $this->assertEquals(array(0,array()), $result);
    }

    public function testArrayToObjectsOneCorrectObject()
    {
        $voucher = \Sdk\Voucher\Utils\MockFactory::generateVoucherArray();
        $data =  $voucher['data'];

        $actual = $this->stub->arrayToObjects($voucher);

        $expectArray = array();

        $expectObject = new Voucher();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes);

        $expectArray = [1, [$data['id']=>$expectObject]];

        $this->assertEquals($expectArray, $actual);
    }

    /**
     * 如果传参错误对象, 期望返回空数组
     */
    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->stub->objectToArray(null);
        $this->assertEquals(array(), $result);
    }
    /**
     * 传参正确对象, 返回对应数组
     */
    // public function testObjectToArrayCorrectObject()
    // {
    //     $voucher = \Sdk\Voucher\Utils\MockFactory::generateVoucherObject(1, 1);

    //     $actual = $this->stub->objectToArray($voucher);

    //     $expectedArray = array(
    //         'data'=>array(
    //             'type'=>'vouchers'
    //         )
    //     );

    //     $expectedArray['data']['attributes'] = array(
    //         'category'=>$voucher->getCategory(),
    //         'voucher'=>$voucher->getVoucher(),
    //         'createTime'=>$voucher->getCreateTime(),
    //         'updateTime'=>$voucher->getUpdateTime(),
    //         'status'=>$voucher->getStatus()
    //     );

    //     $expectedArray['data']['relationships']['crew']['data'] = array(
    //         array(
    //             'type' => 'crews',
    //             'id' =>  $voucher->getCrew()->getId()
    //         )
    //     );
        
    //     $this->assertEquals($expectedArray, $actual);
    // }
}
