<?php
namespace Sdk\Voucher\Utils;

use Sdk\Voucher\Model\Voucher;

use Sdk\Common\Model\IEnableAble;

class MockFactory
{
    /**
     * [generateVoucherArray 生成凭证信息数组]
     * @return [array] [凭证信息]
     */
    public static function generateVoucherArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $voucher = array();

        $voucher = array(
            'data'=>array(
                'type'=>'vouchers',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //category
        $category = self::generateCategory($faker, $value);
        $voucher['category'] = $category;
        //voucher
        $vouchers = self::generateVoucher($faker, $value);
        $voucher['voucher'] = $voucher;
        //crew
        $crew = self::generateCrew($faker, $value);
        $voucher['crew'] = $crew;
        //status
        $status = self::generatestatus($faker, $value);
        $voucher['status'] = $status;

        $voucher['data']['attributes'] = $attributes;
        
        return $voucher;
    }
    /**
     * [generateVoucherObject 生成凭证信息对象]
     * @param  int|integer $id
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]             [凭证信息]
     */
    public static function generateVoucherObject(int $id = 0, int $seed = 0, array $value = array()) : Voucher
    {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $voucher = new Voucher($id);

        //category
        $category = self::generateCategory($faker, $value);
        $voucher->setCategory($category);
        //voucher
        $vouchers = self::generateVoucher($faker, $value);
        $voucher->setVoucher($vouchers);
        //crew
        $crew = self::generateCrew($faker, $value);
        $voucher->setCrew($crew);
        //status
        $status = self::generatestatus($faker, $value);
        $voucher->setStatus($status);

        return $voucher;
    }

    private static function generateCategory($faker, array $value = array())
    {
        return $category = isset($value['category']) ? $value['category'] : 1;
    }

    private static function generateVoucher($faker, array $value = array())
    {
        return $voucher = isset($value['voucher']) ? $value['voucher'] : array('name'=>'voucherName','identify'=>'voucherName.doc');
    }

    private static function generateCrew($faker, array $value = array())
    {
        return $crew = isset($value['crew']) ?
            $value['crew'] : \Sdk\Crew\Utils\MockFactory::generateCrewObject(
                $faker->numerify(),
                $faker->numerify()
            );
    }

    private static function generatestatus($faker, array $value = array())
    {
        return $status = isset($value['status']) ? $value['status'] : 0;
    }
}
