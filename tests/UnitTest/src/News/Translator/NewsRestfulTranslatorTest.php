<?php
namespace Sdk\News\Translator;

use Sdk\Crew\Model\Crew;
// use News\Model\CategoryModelFactory;
use Sdk\News\Model\News;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Sdk\Crew\Translator\CrewRestfulTranslator;
use Sdk\News\Model\NullNews;

class NewsRestfulTranslatorTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(NewsRestfulTranslator::class)
                ->setMethods([
                    'getCrewRestfulTranslator'
                ])
                ->getMock();

        $this->childStub = new class extends NewsRestfulTranslator {
            public function getCrewRestfulTranslator() : CrewRestfulTranslator
            {
                return parent::getCrewRestfulTranslator();
            }
        };
    }

    public function testGetCrewRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Crew\Translator\CrewRestfulTranslator',
            $this->childStub->getCrewRestfulTranslator()
        );
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new NullNews());
        $this->assertInstanceOf('Sdk\News\Model\NullNews', $result);
    }

    public function testArrayToObjectCorrectObject()
    {
        $news = \Sdk\News\Utils\ArrayGenerate::generateNews();

        $data =  $news['data'];

        $actual = $this->childStub->arrayToObject($news);

        $expectObject = new News();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';
        if (isset($attributes['title'])) {
            $expectObject->setTitle($attributes['title']);
        }
        if (isset($attributes['createTime'])) {
            $expectObject->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $expectObject->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['content'])) {
            $expectObject->setContent($attributes['content']);
        }
        if (isset($attributes['category'])) {
            $expectObject->setCategory($attributes['category']);
        }
        if (isset($attributes['image'])) {
            $expectObject->setImage($attributes['image']);
        }

        if (isset($attributes['attachment'])) {
            $expectObject->setAttachments($attributes['attachment']);
        }
        if (isset($attributes['status'])) {
            $expectObject->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $expectObject->setStatusTime($attributes['statusTime']);
        }

        $this->assertEquals($expectObject, $actual);
    }

    // public function testArrayToObjects()
    // {
    //     $result = $this->childStub->arrayToObject(array());
    //     $this->assertEquals(array(), $result);
    // }

    public function testArrayToObjectsOneCorrectObject()
    {
        $news = \Sdk\News\Utils\ArrayGenerate::generateNews();
        $data =  $news['data'];

        $actual = $this->childStub->arrayToObject($news);
        $expectArray = array();

        $expectObject = new News();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';
        if (isset($attributes['title'])) {
            $expectObject->setTitle($attributes['title']);
        }
        if (isset($attributes['createTime'])) {
            $expectObject->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $expectObject->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['content'])) {
            $expectObject->setContent($attributes['content']);
        }
        if (isset($attributes['category'])) {
            $expectObject->setCategory($attributes['category']);
        }
        if (isset($attributes['image'])) {
            $expectObject->setImage($attributes['image']);
        }
        if (isset($attributes['attachments'])) {
            $expectObject->setAttachments($attributes['attachments']);
        }
        if (isset($attributes['status'])) {
            $expectObject->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $expectObject->setStatusTime($attributes['statusTime']);
        }

        $expectArray = [1, [$data['id']=>$expectObject]];

        $this->assertEquals($expectArray, $actual);
    }

    // public function testArrayToObjectsCorrectObject()
    // {
    //     $news[] = \News\Utils\ArrayGenerate::generateNews(1);
    //     $news[] = \News\Utils\ArrayGenerate::generateNews(2);

    //     $newsArray= array('data'=>array(
    //         $news[0]['data'],
    //         $news[1]['data']
    //     ));

    //     $expectArray = array();
    //     $results = array();

    //     foreach ($newsArray['data'] as $each) {
    //         $data =  $each;
    //         $relationships = $data['relationships'];

    //         $expectObject = new News();

    //         $expectObject->setId($data['id']);

    //         $attributes = isset($data['attributes']) ? $data['attributes'] : '';
    //         if (isset($attributes['title'])) {
    //             $expectObject->setTitle($attributes['title']);
    //         }
    //         if (isset($attributes['source'])) {
    //             $expectObject->setSource($attributes['source']);
    //         }
    //         if (isset($attributes['createTime'])) {
    //             $expectObject->setCreateTime($attributes['createTime']);
    //         }
    //         if (isset($attributes['updateTime'])) {
    //             $expectObject->setUpdateTime($attributes['updateTime']);
    //         }
    //         if (isset($attributes['content'])) {
    //             $expectObject->setContent($attributes['content']);
    //         }
    //         if (isset($attributes['parentCategory'])) {
    //             $expectObject->setParentCategory(
    //                 CategoryModelFactory::create($attributes['parentCategory'], TYPE['NEWS_PARENT_CATEGORY_CN'])
    //             );
    //         }
    //         if (isset($attributes['category'])) {
    //             $expectObject->setCategory(
    //                 CategoryModelFactory::create($attributes['category'], TYPE['NEWS_CATEGORY_CN'])
    //             );
    //         }
    //         if (isset($attributes['newsType'])) {
    //             $expectObject->setNewsType(
    //                 CategoryModelFactory::create($attributes['newsType'], TYPE['NEWS_TYPE_CN'])
    //             );
    //         }
    //         if (isset($attributes['dimension'])) {
    //             $expectObject->setDimension($attributes['dimension']);
    //         }
    //         if (isset($attributes['image'])) {
    //             $expectObject->setImage($attributes['image']);
    //         }
    //         if (isset($attributes['attachments'])) {
    //             $expectObject->setAttachments($attributes['attachments']);
    //         }
    //         if (isset($attributes['status'])) {
    //             $expectObject->setStatus($attributes['status']);
    //         }
    //         if (isset($attributes['statusTime'])) {
    //             $expectObject->setStatusTime($attributes['statusTime']);
    //         }
    //         if (isset($attributes['reason'])) {
    //             $expectObject->setReason($attributes['reason']);
    //         }

    //         if (isset($relationships['publishUserGroup']['data'])) {
    //             $expectObject->setPublishUserGroup(new UserGroup($relationships['publishUserGroup']['data']['id']));
    //         }

    //         $results[$data['id']] = $expectObject;
    //     }

    //     $userGroupRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);

    //     $userGroup = new UserGroup($newsArray['data'][0]['relationships']['publishUserGroup']['data']['id']);
    //     $userGroupRestfulTranslator->arrayToObject(
    //         Argument::exact(
    //             $newsArray['data'][0]['relationships']['publishUserGroup']
    //         )
    //     )->shouldBeCalledTimes(1)->willReturn($userGroup);

    //     $userGroup1 = new UserGroup($newsArray['data'][1]['relationships']['publishUserGroup']['data']['id']);
    //     $userGroupRestfulTranslator->arrayToObject(
    //         Argument::exact(
    //             $newsArray['data'][1]['relationships']['publishUserGroup']
    //         )
    //     )->shouldBeCalledTimes(1)->willReturn($userGroup1);

    //     $this->translator->expects($this->exactly(2))
    //         ->method('getUserGroupRestfulTranslator')
    //         ->willReturn($userGroupRestfulTranslator->reveal());

    //     $actual = $this->translator->arrayToObjects($newsArray);

    //     $expectArray = [2, $results];

    //     $this->assertEquals($expectArray, $actual);
    // }
    // /**
    //  * 如果传参错误对象, 期望返回空数组
    //  */
    // public function testObjectToArrayIncorrectObject()
    // {
    //     $result = $this->translator->objectToArray(null);
    //     $this->assertEquals(array(), $result);
    // }
    // /**
    //  * 传参正确对象, 返回对应数组
    //  */
    // public function testObjectToArrayCorrectObject()
    // {
    //     $news = \News\Utils\ObjectGenerate::generateNews(1, 1);

    //     $actual = $this->translator->objectToArray($news);

    //     $expectedArray = array(
    //         'data'=>array(
    //             'type'=>'news',
    //             'id'=>$news->getId()
    //         )
    //     );

    //     $expectedArray['data']['attributes'] = array(
    //         'title'=>$news->getTitle(),
    //         'source'=>$news->getSource(),
    //         'content'=>$news->getContent(),
    //         'parentCategory'=>$news->getParentCategory()->getId(),
    //         'category'=>$news->getCategory()->getId(),
    //         'newsType'=>$news->getNewsType()->getId(),
    //         'image'=>$news->getImage(),
    //         'attachments'=>$news->getAttachments(),
    //         'dimension' =>$news->getDimension(),
    //         'rejectReason'=>$news->getReason()
    //     );

    //     $expectedArray['data']['relationships']['applyCrew']['data'] = array(
    //         array(
    //             'type' => 'crews',
    //             'id' => $news->getCrew()->getId()
    //         )
    //     );

    //     $this->assertEquals($expectedArray, $actual);
    // }
}
