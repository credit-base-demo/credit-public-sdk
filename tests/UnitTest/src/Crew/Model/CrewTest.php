<?php
namespace Sdk\Crew\Model;

use Sdk\Crew\Repository\CrewRepository;
use Sdk\Crew\Repository\CrewSessionRepository;

use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Common\Adapter\IEnableAbleAdapter;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

class CrewTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(Crew::class)
            ->setMethods([
                'getRepository',
                'IOperatAbleAdapter',
                'IEnableAbleAdapter'
            ])->getMock();

        $this->childStub = new Class extends Crew{

            public function getRepository() : CrewRepository
            {
                return parent::getRepository();
            }

            public function getIOperatAbleAdapter() : IOperatAbleAdapter
            {
                return parent::getIOperatAbleAdapter();
            }

            public function getIEnableAbleAdapter() : IEnableAbleAdapter
            {
                return parent::getIEnableAbleAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Crew\Repository\CrewRepository',
            $this->childStub->getRepository()
        );
    }

    public function testExtendsUser()
    {
        $this->assertInstanceOf('Sdk\User\Model\User', $this->stub);
    }

    public function testCorrectImplementsIObject()
    {
        $this->assertInstanceof('Marmot\Common\Model\IObject', $this->stub);
    }

    public function testCorrectImplementsIOperatAble()
    {
        $this->assertInstanceof('Sdk\Common\Model\IOperatAble', $this->stub);
    }

    public function testCorrectImplementsIEnableAble()
    {
        $this->assertInstanceof('Sdk\Common\Model\IEnableAble', $this->stub);
    }

    public function testGetIOperatAble()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\IOperatAbleAdapter',
            $this->childStub->getIOperatAbleAdapter()
        );
    }

    public function testGetIEnabledAble()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\IEnableAbleAdapter',
            $this->childStub->getIEnableAbleAdapter()
        );
    }

    public function signIn()
    {
        if ($this->getRepository()->signIn($this)) {
            return true;
        }

        return false;
    }

    public function testUpdatePassword()
    {
        $repository = $this->prophesize(CrewRepository::class);
        $repository->updatePassword(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->updatePassword();
        $this->assertTrue($result);
    }
}
